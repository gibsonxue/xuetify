import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/MainPage.vue'
import LoginPage from '../views/LoginPage.vue'
import DataView from '../views/DataView.vue'
import HostView from '../views/HostView.vue'
import Apps from '../views/Apps.vue'
import KubernetesView from '../views/KuberentesView.vue'
import ElasticView from '../views/ElasticView.vue'

Vue.use(VueRouter)
    const routes = [
        {
         path: '/login',
         name: 'login',
         component: LoginPage,
        },
        {
         path: '/',
         name: 'Home',
         component: Home,
         children: [
            {
            path: 'apps/:appcatagory/',
            name: 'Apps',
            component: Apps
           },{
            path: 'dataview',
            name: 'DataView',
            component: DataView
           },{
            path: 'hostview',
            name: 'Host',
            component: HostView
           },{
             path: 'k8s',
             name: 'K8s',
             component: KubernetesView
             },
             {
             path: 'elastic',
             name: 'elastic',
             component: ElasticView
             }

         ]
      },
    ]

const router = new VueRouter({
  mode: 'history',
  routes
})


router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/login', '/register'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');

  if (authRequired && !loggedIn) {
    return next('/login');
  }
  next();
})


export default router

const originalPush = VueRouter.prototype.push
   VueRouter.prototype.push = function push(location) {
   return originalPush.call(this, location).catch(err => err)
}