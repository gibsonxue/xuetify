import { Axios } from '../plugins/AxiosPlugin'


const logs_init = {
	resp : {
		hits: {
			total: {
				value: 0
			},
            hits: []
		},
		aggregations: {
			by_time: {
				buckets:  [{
					"key": 1631895376000,
				}],
			}
		}
	}
}


const state = {
   indexList: [],
   logs: logs_init,
   headers: {},
}
// getters
const getters = {
  }

const actions = {
    GetIndexList({commit}) {
        Axios.get('index/')
            .then(response => {
                commit('mutateIndexList', { indexes: response.data } )
            })
            .catch(function (error) {
                console.log(error)
            })
    },
    GetHeader({commit},params) {
        Axios.get('header/' + params.id + '/')
            .then(response => {
                commit('mutateheader', { headers: response.data })
            })
            .catch(function (error) {
                console.log(error)
            })
    },
        GetLogs({commit}, params) {
        Axios.get('logs/' + params.selected_index + '/' , { params: params} )
            .then(response => {
                if (response.data.resp.aggregations.by_time.buckets.length != 0) {
                    commit('mutateLogList', {logs: response.data})
                } else {
                    commit('mutateLogList', {logs: logs_init})
                }
            })
            .catch(function (error) {
                console.log(error)
            })
    }
}

//   CreateHost({ dispatch }, data,){
//     Axios.post('host/',data)
//       .then(response => {
//         dispatch('GetHostInfo')
//       })
//       .catch(function (error) {
//         console.log(error)
//       })
//   },
//   UpdateHostGroup({ dispatch }, data){
//     Axios.put('http://127.0.0.1:8000/host/' + data.id + '/', data)
//       .then(response => {
//         console.log(JSON.stringify(response.data))
//         dispatch('GetHostInfo')
//       })
//       .catch(function (error) {
//         console.log(error)
//       })
//   },
//   DeleteHost({dispatch}, data){
//       Axios.delete('http://127.0.0.1:8000/host/' + data.id + '/')
//       .then(response => {
//         dispatch('GetHostInfo')
//       })
//       .catch(function (error) {
//         console.log(error)
//       })
//   }
// }

const mutations = {
    mutateIndexList (state, { indexes }) {
      state.indexList = indexes
    },
   mutateLogList (state, { logs }) {
      state.logs = logs
    },
   mutateheader (state, { headers }) {
      state.headers = headers
    }
  }

export default {
  name: 'es',
  namespaced: true,
  state,
  getters,
  mutations,
  actions,

}