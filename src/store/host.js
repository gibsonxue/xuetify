import { Axios } from '../plugins/AxiosPlugin'

const state = {
   hosts: [],
}
// getters
const getters = {
  }

const actions = {
  GetHostInfo({ commit }) {
    Axios.get('host/')
      .then(response => {
        commit('mutateHostInfo', {hosts: response.data}
      )})
      .catch(function (error) {
        console.log(error)
      })
  },
  CreateHost({ dispatch }, data,){
    Axios.post('host/',data)
      .then(response => {
        dispatch('GetHostInfo')
      })
      .catch(function (error) {
        console.log(error)
      })
  },
  UpdateHostGroup({ dispatch }, data){
    Axios.put('http://127.0.0.1:8000/host/' + data.id + '/', data)
      .then(response => {
        console.log(JSON.stringify(response.data))
        dispatch('GetHostInfo')
      })
      .catch(function (error) {
        console.log(error)
      })
  },
  DeleteHost({dispatch}, data){
      Axios.delete('http://127.0.0.1:8000/host/' + data.id + '/')
      .then(response => {
        dispatch('GetHostInfo')
      })
      .catch(function (error) {
        console.log(error)
      })
  }
}

const mutations = {
    mutateHostInfo (state, { hosts }) {
      state.hosts = hosts
    }
  }

export default {
  name: 'host',
  namespaced: true,
  state,
  getters,
  mutations,
  actions,

}