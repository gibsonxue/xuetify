/**
 * Created by root on 6/4/21.
 */
import Axios from 'axios'
import authHeader from '../services/auth-header';

const state = {
    DeploymentList :[],
    deployment_params: {
        deployment_name: '',
        namespace: '',
        deployment_type: '',
        replica: ""
    },
    deployment_container: {}
}

// getters
const getters = {
    full_map: (state, getters) => {
        var deployment_name = state.deployment_params.deployment_name
        return {
            "apiVersion": "apps/v1",
            "kind": "Deployment",
            "metadata": {
                "labels": {
                    "k8s-app": deployment_name
                },
                "name": deployment_name,
                "namespace": "default"
            },
            "spec": {
                "replicas": 1,
                "selector": {
                    "matchLabels": {
                        "k8s-app": deployment_name
                    }
                },
                "template": {
                    "metadata": {
                        "labels": {
                            "k8s-app": deployment_name
                        }
                    },
                    "spec": {
                        "nodeSelector": {
                            "node-role.kubernetes.io/edge": ""
                        },
                        "containers": [{
                            "name": "kubeedge-pi-redis-backend",
                            "image": "arm64v8/redis",
                            "imagePullPolicy": "IfNotPresent",
                            "ports": [{
                                "containerPort": 8086,
                                "hostPort": 8086
                            }]
                        },
                            {
                                "name": "kubeedge-pi-redis-frontend",
                                "image": "arm64v8/nginx",
                                "imagePullPolicy": "IfNotPresent",
                                "ports": [{
                                    "containerPort": 8086,
                                    "hostPort": 8086
                                }]
                            }
                        ],
                        "restartPolicy": "Always"
                    }
                }
            }
        }
    }
}

const actions = {
    create_params({ commit }, params ){
        commit('update_params',params)
    },
    create_dockers({ commit }, params ){
        commit('update_dockers',params)
    }
}

const mutations = {
    update_params(state, params){
        state.deployment_params = params
        console.log(JSON.stringify(state.deployment_params))
    },
    update_dockers(state, dockers){
        state.deployment_container = dockers
        console.log(JSON.stringify(state))
    }
}

export default {
    name: 'deployment',
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}