import Axios from 'axios'

const state = {
   app_list: [],
   app_detail: [],
   // app_groups: [],
   app_configs: [],
   app_catagory: {}
}

const getters = {
  }

const actions = {
   GetCatagoryInfo({commit},id){
      Axios.get('http://127.0.0.1:8000/appcatagory/' + id + '/')
       .then(response => {
        commit('updateAppCatagory', { app_ctg: response.data})
      })
      .catch(function (error) {
        console.log(error)
      })
   },
   GetAppDetails({ commit},id ){
    Axios.get('http://127.0.0.1:8000/apps/' + id +'/')
      .then(response => {
        commit('updateApps', { app_detail: response.data})
      })
      .catch(function (error) {
        console.log(error)
      }),
    Axios.get('http://127.0.0.1:8000/appconfig/?app=' + id)
      .then(response => {
        let config_list = []
        for (var i in response.data.results){
                config_list.push(response.data.results[i].text)
        }
        commit('updateConfig', { app_configs: config_list})
      })
      .catch(function (error) {
        console.log(error)
      })},
    GetAppList({ commit }, item ){
       Axios.get('http://127.0.0.1:8000/apps', {params: item })
       .then(response => {
        commit('updateAppList', { app_list: response.data})
      })
      .catch(function (error) {
        console.log(error)
      })
   },
    CreateApp( {dispatch}, data){
       Axios.post('http://localhost:8000/apps',data)
            .then(response => {
              // console.log(JSON.stringify(response.data.results))
/*              dispatch('GetAppList')*/
          })
          .catch(function (error) {
              console.log(error)
          })
     },
     UpdateApp( {dispatch}, data){
       console.log(JSON.stringify(data))
       Axios.put('http://localhost:8000/apps/' + data.id + '/',data)
            .then(response => {
              dispatch('GetAppList')
              console.log(JSON.stringify(response.data))
          })
          .catch(function (error) {
              console.log(error)
          })
     },
 }

const mutations = {
    // updateAppGroups (state, { groups }) {
    //   state.app_groups = groups
    // },
    updateConfig (state, { app_configs }) {
      state.app_configs = app_configs
      // console.log(JSON.stringify(app_config))
    },
    updateApps (state, { app_detail }) {
      state.app_detail = app_detail
      // console.log(JSON.stringify(apps_detail))
    },
    updateAppList (state, { app_list }) {
      state.app_list = app_list
      // console.log(JSON.stringify(app_list))
    },
    updateAppCatagory (state, { app_ctg }) {
      state.app_catagory = app_ctg
      console.log(JSON.stringify(app_ctg))
    },

  }
export default {
  name: 'apps',
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}