import Vue from 'vue'
import Vuex from 'vuex'
import dessert from './dessert'
import { auth } from './auth';
import  host  from './host';
import group from './group'
import apps from './apps'
import deployment from './deployment'
import es from './es'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isLoading: false
  },
  mutations: {
  },
  actions: {
    getsoon(){
      console.log('getsoon')
    }
  },
  modules: {
    apps,
    host,
    dessert,
    auth,
    group,
    deployment,
    es
  }
})
